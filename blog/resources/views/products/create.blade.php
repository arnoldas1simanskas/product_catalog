@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
                    @csrf
                    <label>Title:</label>
                    <input class="form-control" name="title" type="text">
                    <label>Description:</label>
                    <textarea id="example" class="form-control" name="description"></textarea>
                    <label>Base price:</label>
                    <input class="form-control" name="base_price" type="text">
                    <label>Individual discount:</label>
                    <input class="form-control" name="individual_discount" type="text">
                    <label>Item image:</label>
                    <input class="form-control" name="image" type="file">
                    <label>Status:</label>
                    <select class="form-control" name="status" required>
                        <option selected value="enabled">Enabled</option>
                        <option value="disabled">Disabled</option>
                    </select>
                    <input type="submit" class="btn btn-primary" value="Add new product item">
                </form>
            </div>
        </div>
    </div>
@endsection