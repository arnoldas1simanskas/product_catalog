@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('products.update', [$product->id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <label>Title:</label>
                    <input class="form-control" name="title" type="text" value="{{ $product->title }}">
                    <label>Description:</label>
                    <textarea id="example" class="form-control" name="description">{{ $product->description }}</textarea>
                    <label>Base price:</label>
                    <input class="form-control" name="base_price" type="text" value="{{ $product->base_price }}">
                    <label>Individual discount:</label>
                    <input class="form-control" name="individual_discount" type="text" value="{{ $product->individual_discount }}">
                    <label>Item image:</label>
                    <input class="form-control" name="image" type="file">
                    <label>Status: {{ $product->status }}</label>
                    <select class="form-control" name="status" required>
                        <option value="enabled">Enabled</option>
                        <option value="disabled">Disabled</option>
                    </select>
                    <input type="submit" class="btn btn-primary" value="Update product item">
                </form>
            </div>
        </div>
    </div>
@endsection