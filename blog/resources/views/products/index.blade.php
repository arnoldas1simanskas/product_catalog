@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @include('components.products-item')
            @include('components.admin-product-management')
        </div>
        <div>
            {{ $products->links() }}
        </div>
    </div>
@endsection