@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div>
                    <h2>{{ $product->title }}</h2>
                    @if(isset($product->image))
                        @if(strpos($product->image, "https://") !== false || (strpos($product->image, "http://") !== false))
                            <img src="{{ $product->image }}" style="max-width: 500px; max-height: 500px;" />
                        @else
                            <img src="{{ Storage::url($product->image) }}" style="max-width: 500px; max-height: 500px;" />
                        @endif
                    @endif
                    <div class="container">
                        <span id="rateMe1"></span>
                    </div>
                    @include('components.ratings')
                    @if(isset($product->individual_discount) || $product->global_discount() > 0)
                        <li>
                            Base price with tax: <s>{{ number_format($product->base_price(), 2) }}</s>€
                        </li>
                        <li>
                            Special price: {{ number_format($product->special_price(), 2) }}€
                        </li>
                    @else
                        <li>
                            Base price with tax: {{ number_format($product->base_price(), 2) }}€
                        </li>
                    @endif
                    <p class="fr-view">{!! $product->description !!}</p>
                </div>
            </div>
            <div class="col-md-6">

                <h2>Comments</h2>
                @include('components.comments-form')
                @include('components.comments-list')
            </div>
        </div>
    </div>
@endsection