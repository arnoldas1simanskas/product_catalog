@if($product->comments->count() > 0)
    <div class="border border-dark rounded">
        <h5>{{ $product->comments->count() }} comments wrote.. </h5>
        @foreach($product->comments as $comment)
            <div class="comment mt-3 border border-success rounded">
                Comment by: {{ $comment->user->name }}
                | post in - {{ $comment->created_at }}
                <div>
                    {{ $comment->comment_text }}
                </div>
                <div>
                    @if(isset(Auth::user()->id) && $comment->user_id == Auth::user()->id)
                        <form method="post" action="{{ route('comments.destroy', [$comment->id]) }}">
                            @csrf
                            @method('delete')
                            <input type="submit" value="Delete comment" class="btn btn-danger">
                        </form>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endif