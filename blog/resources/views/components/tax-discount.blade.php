@if(isset(Auth::user()->role) && Auth::user()->role == 'admin')
    <div>
        <form method="post" action="{{ route('taxdiscount.store') }}">
            @csrf
            <div>
                <label>
                    Tax:
                    @if(Session::has('tax'))
                        {{ Session::get('tax') }}%
                    @endif
                </label>
                <input class="small col-sm-12" min="0" max="100" type="number" name="tax" value="{{ $tax }}">
            </div>
            <div>
                <label>
                    Discount:
                    @if(Session::has('global_discount'))
                        {{ Session::get('global_discount') }}%
                    @endif
                </label>
                <input class="small col-sm-12" min="0" max="100" type="number" name="global_discount" value="{{ $global_discount }}">
            </div>
            <input class="small" type="submit" value="Change values">
        </form>
    </div>
@endif