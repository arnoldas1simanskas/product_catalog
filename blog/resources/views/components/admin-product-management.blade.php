@if(isset(Auth::user()->role) && Auth::user()->role == 'admin')
    <div class="col-md-12">
        <div class="row">
            @foreach($products as $item)
                @if(isset($item->taxes()->tax) && isset($item->discounts()->global_discount))
                    <h4>
                        Tax: {{ $item->taxes()->tax }}% | Global discount: -{{ $item->discounts()->global_discount }}%
                    </h4>
                @else
                    <h4>
                        Tax: Not set | Global discount: Not set
                    </h4>
                @endif
                @break
            @endforeach
        </div>
        <div class="row">
            <a href="{{ route('products.create') }}" class="btn btn-success">Create new product item</a>
            <form method="post" action="{{ route('products.destroyAll') }}">
                @csrf
                @method('delete')
                <input class="btn btn-danger" type="submit" value="Delete All">
            </form>
        </div>
    </div>
    <div class="col-md-12">
        <table id="table_test" class="table">
            <thead>
            <th>
                ID
            </th>
            <th>
                Status
            </th>
            <th>
                Title
            </th>
            <th>
                Description
            </th>
            <th>
                Base price without tax
            </th>
            <th>
                Special price
            </th>
            <th>
                Image
            </th>
            <th>
                Edit
            </th>
            <th>
                Delete
            </th>
            </thead>
            <tbody>
            @foreach($products as $item)
                <tr>
                    <td>
                        <a href="{{ route('products.show', [$item->id]) }}">
                            {{ $item->id }}
                        </a>
                    </td>
                    @if($item->status == 'enabled')
                        <td bgcolor="#008000">
                            {{ $item->status }}
                        </td>
                    @else
                        <td bgcolor="#ff0000">
                            {{ $item->status }}
                        </td>
                    @endif
                    <td>
                        {{ $item->title }}
                    </td>
                    <td class="fr-view">
                        {!! substr($item->description,0,150) !!}...
                    </td>
                    @if(isset($item->individual_discount) || $item->global_discount() > 0)
                        <td>
                            {{ number_format($item->base_price, 2) }}€
                        </td>
                        <td>
                            {{ number_format($item->special_price(), 2) }}€
                        </td>
                    @else
                        <td>
                            {{ number_format($item->base_price, 2) }}€
                        </td>
                        <td>
                            Not set
                        </td>
                    @endif

                    <td>
                        @if(isset($item->image))
                            @if(strpos($item->image, "https://") !== false || (strpos($item->image, "http://") !== false))
                                <img src="{{ $item->image }}" style="max-width: 100px; max-height: 100px;" />
                            @else
                                <img src="{{ Storage::url($item->image) }}" style="max-width: 100px; max-height: 100px;" />
                            @endif
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('products.edit', [$item->id]) }}" class="btn btn-success">Edit</a>
                    </td>
                    <td>
                        <form method="post" action="{{ route('products.destroy', [$item->id]) }}">
                            @csrf
                            @method('delete')
                            <input class="btn btn-danger" type="submit" value="X">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif