@if(isset(Auth::user()->id))
    <form method="post" action="{{ route('comments.store') }}">
        @csrf
        <div class="form-group">
            <textarea name="comment_text" class="form-control"></textarea>
        </div>

        <input type="hidden" name="product_id" value="{{ $product->id }}">
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

        <input type="submit" value="Comment" class="btn btn-success">
    </form>
@endif