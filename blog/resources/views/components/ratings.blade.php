<div>
    <li>
        Product ratings: {{ $countRate }}
    </li>
</div>
@if(isset(Auth::user()->id))
    <div>
        @if($ratings == false)
            <form method="post" action="{{ route('ratings.store') }}">
                @csrf
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="product_id" value="{{ $product->id }}">
                <label>Rate from 0-5:</label>
                <input type="number" name="rating" min="0" max="5">
                <input type="submit" value="Rate">
            </form>
        @else
            <p>Thank you for your rating</p>
        @endif
    </div>
@else
    <div>
        <p>You must login to rate.</p>
    </div>
@endif