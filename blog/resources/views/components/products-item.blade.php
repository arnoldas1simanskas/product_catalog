@if(isset(Auth::user()->role) && Auth::user()->role == 'user' || Auth::user() != true)
    @foreach($products as $item)
        <div class="col-md-6">
            <h3>
                <a href="{{ route('products.show', [$item->id]) }}">
                    {{ $item->title }}
                </a>
            </h3>
            @if(isset($item->image))
                @if(strpos($item->image, "https://") !== false || (strpos($item->image, "http://") !== false))
                    <img src="{{ $item->image }}" style="max-width: 100px; max-height: 100px;" />
                @else
                    <img src="{{ Storage::url($item->image) }}" style="max-width: 100px; max-height: 100px;" />
                @endif
            @endif
            @if(isset($item->individual_discount) || $item->global_discount() > 0)
                @if($item->tax() > 0)
                    <p>
                        Base price +tax: <s>{{ number_format($item->base_price(), 2) }}</s>€
                    </p>
                @else
                    <p>
                        Base price: {{ number_format($item->base_price(), 2) }}€
                    </p>
                @endif
                <p>
                    Special price {{ number_format($item->special_price(), 2) }}€
                </p>
            @elseif($item->tax() > 0)
                <p>
                    Base price +tax: {{ number_format($item->base_price(), 2) }}€
                </p>
            @else
                <p>
                    Base price: {{ number_format($item->base_price(), 2) }}€
                </p>
            @endif
            <p class="fr-view">
                {!! substr($item->description,0,300) !!}...
            </p>
        </div>
    @endforeach
@endif