<?php

use App\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $strings = array('enabled', 'disabled');

        for($i =0; $i < 10; $i++){
            $product = new Product();
            $product->title = $faker->sentence(3);
            $product->base_price = $faker->randomFloat(2);
            $product->individual_discount = $faker->randomFloat(2, 0, $product->base_price);
            $key = array_rand($strings);
            $product->status = $strings[$key];
            $product->image = $faker->imageUrl(640, 480, 'technics');
            $product->description = $faker->paragraphs(3, true);

            $product->save();
        }
    }
}
