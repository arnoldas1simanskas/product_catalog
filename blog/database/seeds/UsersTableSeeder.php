<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'test@admin.lt';
        $user->password = bcrypt('secret');
        $user->role = 'admin';
        $user->save();

        $user = new User();
        $user->name = 'User';
        $user->email = 'test@user.lt';
        $user->password = bcrypt('secret');
        $user->role = 'user';
        $user->save();
    }
}
