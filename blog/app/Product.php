<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $guarded =['id'];

    public function ratings(){
        return $this->hasMany('App\Rating', 'product_id', 'id')->get();
    }

    public function comments() {
        return $this->hasMany('App\Comment', 'product_id', 'id')->orderBy( 'created_at', 'DESC' );
    }

    public function taxes(){

        $taxes = Tax::orderBy('created_at', 'desc')->first();

        return $taxes;
    }

    public function discounts(){

        $discounts = GlobalDiscount::orderBy('created_at', 'desc')->first();

        return $discounts;
    }

    public function tax(){

        //$tax_perc = session('tax')/100;
        $default = 0.21;
        if(isset($this->taxes()->tax)){
            $tax_perc = $this->taxes()->tax / 100;
            $tax = $this->base_price * $tax_perc;
        } else {
            $tax = $this->base_price * $default;
        }

        return $tax;
    }

    public function global_discount(){

        $global_discount = 0;
        if(isset($this->discounts()->global_discount)){
//            $discount = session('global_discount')/100;
            $discount = $this->discounts()->global_discount / 100;
            $global_discount = $this->base_price * $discount;
        }


        return $global_discount;
    }

    public function base_price(){

        $base_price = $this->base_price + $this->tax();

        return $base_price;
    }

    public function special_price(){
        $special_price = 0;
        if(isset($this->individual_discount)){
            $special_price = $this->base_price + $this->tax() - $this->individual_discount;
        }elseif($this->individual_discount == NULL && $this->global_discount() > 0){
            $special_price = $this->base_price + $this->tax() - $this->global_discount();
        }

        return $special_price;
    }
}
