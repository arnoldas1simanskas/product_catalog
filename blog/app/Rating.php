<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $table = 'ratings';

    protected $guarded =['id'];
}
