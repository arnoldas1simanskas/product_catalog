<?php

namespace App\Providers;

use App\Product;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        view()->composer('*', function($view) use (&$session) {
//            $product = Product::all();
//            $tax = $product->tax();
//            $global_discount = $product->global_discount;
//            $view->with([
//                'tax' => $tax,
//                'global_discount' =>$global_discount
//            ]);
//        });
        view()->composer('*', function($view) use (&$auth) {
            $product = Product::all();
            $tax = 21;
            $global_discount = 0;
            foreach ($product as $item){
                if(isset($item->id) && isset($item->taxes()->tax)) {
                    $tax = $item->taxes()->tax;
                    $global_discount = $item->discounts()->global_discount;
                    break;
                }
            }

            $view->with([
                'tax' => $tax,
                'global_discount' =>$global_discount
            ]);
        });

        Schema::defaultStringLength(191);
    }
}
