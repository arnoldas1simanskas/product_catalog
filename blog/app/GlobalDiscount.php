<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalDiscount extends Model
{
    protected $table = 'global_discount';
}
