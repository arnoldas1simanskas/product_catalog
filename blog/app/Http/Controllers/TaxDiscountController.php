<?php

namespace App\Http\Controllers;

use App\GlobalDiscount;
use App\Http\Requests\TaxDiscountRequest;
use App\Tax;
use Illuminate\Http\Request;

class TaxDiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxDiscountRequest $request)
    {

        $taxIn = new Tax();
        $discount = new GlobalDiscount();

        $tax = request('tax');
        if(isset($tax)){
            session()->put('tax', $tax);
            $taxIn->tax = session('tax');
            $taxIn->save();
        }

        $global_discount = request('global_discount');
        if(isset($global_discount)){
            session()->put('global_discount', $global_discount);
            $discount->global_discount = session('global_discount');
            $discount->save();
        }

        //dd(session('tax'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
