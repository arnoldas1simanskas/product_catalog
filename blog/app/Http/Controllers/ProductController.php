<?php

namespace App\Http\Controllers;

use App\GlobalDiscount;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Rating;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset(Auth::user()->role) && Auth::user()->role == 'user' || Auth::user() != true){
            $products = Product::where('status', '=', 'enabled')->paginate(8);
        } elseif(isset(Auth::user()->role) && Auth::user()->role == 'admin') {
            $products = Product::paginate(8);
        }

        return view('products.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request, Product $product)
    {
        $input = $request->except(['image']);

        if($request->hasFile('image')){
            $imagePath = $request->file('image')->store('product-images');
            $product->image = $imagePath;
        }

        $product->fill($input)->save();

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $rating = Rating::where('product_id', '=', $product->id)->get();

        $ratings = false;

        if(isset(Auth::user()->id)){
            foreach ($rating as $r){
                if($r->user_id == Auth::user()->id){
                    $ratings = true;
                }
            }
        }
//dd(session('tax'));

        $totalRate = 0;
        $i = 0;
        $sum = 0;
        $countRate = 0;

        foreach ($rating as $rate){
            $totalRate += $rate->rating;
            $sum = ++$i;
        }
        if($totalRate > 0){
            $countRate = number_format(($totalRate / $sum), 1);
        }




        return view('products.show', [
            'product' => $product,
            'countRate' => $countRate,
            'ratings' => $ratings,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', [
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->except(['image']);

        if($request->hasFile('image')){
            Storage::delete($product->image);
            $imagePath = $request->file('image')->store('product-images');
            $product->image = $imagePath;
        }

        $product->fill($input)->save();

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Storage::delete($product->image);
        $product->delete();

        return redirect()->route('products.index');
    }

    public function destroyAll(Request $request)
    {
        $file = new Filesystem;

        $file->cleanDirectory('../storage/app/public/product-images');
        Product::where('id', '>', -1)->delete();

        return redirect()->route('products.index');
    }

}
