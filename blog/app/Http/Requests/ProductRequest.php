<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:30',
            'description' => 'required|min:10|max:500',
            'base_price' => 'required|numeric|not_in:0|min:0',
            'individual_discount' => 'nullable|numeric|not_in:0|min:0',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20048',
            'status' => 'required|in:enabled,disabled',
        ];
    }
}
