<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::delete('products/destroyAll', 'ProductController@destroyAll')->name('products.destroyAll');
Route::resource('products', 'ProductController');
Route::resource('ratings', 'RatingController');
Route::resource('comments', 'CommentController');
Route::resource('taxdiscount', 'TaxDiscountController');

